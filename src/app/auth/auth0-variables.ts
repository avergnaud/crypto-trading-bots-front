interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'nxP8SOjRRx7ZH7jZnhzOrXA91m1VbOE1',
  domain: 'brochain.eu.auth0.com',
  callbackURL: 'http://localhost:4200/callback'
};
