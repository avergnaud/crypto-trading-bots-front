import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Bot } from '../bot';
import { BotService } from '../bot.service';
import { AuthService } from './../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private botService: BotService, public auth: AuthService) { }

  ngOnInit() {
  }

}
