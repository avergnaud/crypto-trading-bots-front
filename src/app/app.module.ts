import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { AppRoutingModule } from './app-routing.module';

import { AuthService } from './auth/auth.service';
import { CallbackComponent } from './callback/callback.component';
import { BotsComponent } from './bots/bots.component';
import { BotDetailComponent } from './bot-detail/bot-detail.component';
import { MessagesComponent } from './messages/messages.component';

import { MessageService } from './message.service';
import { BotService } from './bot.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CallbackComponent,
    BotsComponent,
    BotDetailComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [BotService, MessageService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
