import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Bot } from '../bot';
import { BotService } from '../bot.service';
import { AuthService } from './../auth/auth.service';

@Component({
  selector: 'app-bots',
  templateUrl: './bots.component.html',
  styleUrls: ['./bots.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BotsComponent implements OnInit {
  bots: Bot[];

  constructor(private botService: BotService, public auth: AuthService) { }

  ngOnInit() {
    this.getBots();
  }

  getBots(): void {
    this.botService.getBots()
    .subscribe(bots => this.bots = bots);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.botService.addBot({ name } as Bot)
      .subscribe(bot => {
        this.bots.push(bot);
      });
  }

  delete(bot: Bot): void {
    this.bots = this.bots.filter(h => h !== bot);
    this.botService.deleteBot(bot).subscribe();
  }
}
