import { Component, OnInit, Input } from '@angular/core';
import { Bot } from '../bot';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { BotService }  from '../bot.service';

@Component({
  selector: 'app-bot-detail',
  templateUrl: './bot-detail.component.html',
  styleUrls: ['./bot-detail.component.css']
})
export class BotDetailComponent implements OnInit {

  /**
   * The bot property must be an Input property, annotated with the @Input() decorator, because the external BotsComponent will bind to it like this.
   * <app-bot-detail [bot]="selectedBot"></app-bot-detail>
   */
  @Input() bot: Bot;

  constructor(
    private route: ActivatedRoute,
    private botService: BotService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getBot();
  }
  
  getBot(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.botService.getBot(id)
      .subscribe(bot => this.bot = bot);
  }

  goBack(): void {
    this.location.back();
  }
  
  save(): void {
    this.botService.updateBot(this.bot)
      .subscribe(() => this.goBack());
  }

}
