import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Bot } from './bot';
import { MessageService } from './message.service';

const httpOptions = { 
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class BotService {

  private botsUrl = 'http://localhost:3001/api/v1/bots';
  
    constructor(
      private http: HttpClient,
      private messageService: MessageService) { }
  
    /** GET bots from the server */
    getBots (): Observable<Bot[]> {
      return this.http.get<Bot[]>(this.botsUrl)
        .pipe(
          tap(bots => this.log(`fetched bots`)),
          catchError(this.handleError('getBots', []))
        );
    }
  
    /** GET bot by id. Return `undefined` when id not found */
    getBotNo404<Data>(id: number): Observable<Bot> {
      const url = `${this.botsUrl}/?id=${id}`;
      return this.http.get<Bot[]>(url)
        .pipe(
          map(bots => bots[0]), // returns a {0|1} element array
          tap(h => {
            const outcome = h ? `fetched` : `did not find`;
            this.log(`${outcome} bot id=${id}`);
          }),
          catchError(this.handleError<Bot>(`getBot id=${id}`))
        );
    }
  
    /** GET bot by id. Will 404 if id not found */
    getBot(id: number): Observable<Bot> {
      const url = `${this.botsUrl}/${id}`;
      return this.http.get<Bot>(url).pipe(
        tap(_ => this.log(`fetched bot id=${id}`)),
        catchError(this.handleError<Bot>(`getBot id=${id}`))
      );
    }
  
    //////// Save methods //////////
  
    /** POST: add a new bot to the server */
    addBot (bot: Bot): Observable<Bot> {
      return this.http.post<Bot>(this.botsUrl, bot, httpOptions).pipe(
        tap((bot: Bot) => this.log(`added bot w/ id=${bot.id}`)),
        catchError(this.handleError<Bot>('addBot'))
      );
    }
  
    /** DELETE: delete the bot from the server */
    deleteBot (bot: Bot | number): Observable<Bot> {
      const id = typeof bot === 'number' ? bot : bot.id;
      const url = `${this.botsUrl}/${id}`;
  
      return this.http.delete<Bot>(url, httpOptions).pipe(
        tap(_ => this.log(`deleted bot id=${id}`)),
        catchError(this.handleError<Bot>('deleteBot'))
      );
    }
  
    /** PUT: update the bot on the server */
    updateBot (bot: Bot): Observable<any> {
      return this.http.put(this.botsUrl, bot, httpOptions).pipe(
        tap(_ => this.log(`updated bot id=${bot.id}`)),
        catchError(this.handleError<any>('updateBot'))
      );
    }
  
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
  
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead
  
        // TODO: better job of transforming error for user consumption
        this.log(`${operation} failed: ${error.message}`);
  
        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }
  
    /** Log a BotService message with the MessageService */
    private log(message: string) {
      this.messageService.add('BotService: ' + message);
    }

}
