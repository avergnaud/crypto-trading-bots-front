import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BotsComponent }      from './bots/bots.component';
import { BotDetailComponent }  from './bot-detail/bot-detail.component';
import { HomeComponent } from './home/home.component';
import { CallbackComponent } from './callback/callback.component';

const routes: Routes = [
  { path: '', redirectTo: '/bots', pathMatch: 'full' },
  { path: 'bots', component: BotsComponent },
  { path: 'detail/:id', component: BotDetailComponent },
  { path: 'callback', redirectTo: '/bots' },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }